/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;
import java.time.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author TIASS
 */
public class Produit {
    private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;
    private ArrayList<ProduitAchete> produitAchete;

    public Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
    }
    
    public boolean estPerime(){
        return datePeremption.isAfter(LocalDate.now());
    }
            
    public boolean estPerime(LocalDate ref){
        return datePeremption.isAfter(ref);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Produit)) return false;
        Produit produit = (Produit) o;
        return getId() == produit.getId() && Double.compare(produit.getPrixUnitaire(), getPrixUnitaire()) == 0 && Objects.equals(getLibelle(), produit.getLibelle()) && Objects.equals(getDatePeremption(), produit.getDatePeremption()) && Objects.equals(getCategorie(), produit.getCategorie()) && Objects.equals(produitAchete, produit.produitAchete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLibelle(), getPrixUnitaire(), getDatePeremption(), getCategorie(), produitAchete);
    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", prixUnitaire=" + prixUnitaire +
                ", datePeremption=" + datePeremption +
                ", categorie=" + categorie +
                ", produitAchete=" + produitAchete +
                '}';
    }
}
