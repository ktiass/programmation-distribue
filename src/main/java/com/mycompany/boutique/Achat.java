/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author TIASS
 */
public class Achat {
    private long id;
    private double remise = 0;
    private LocalDate dateAchat;
    private ArrayList<ProduitAchete> produitsAchetes;

    public Achat(long id, LocalDate dateAchat, ArrayList<ProduitAchete> produitsAchetes) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.produitsAchetes = produitsAchetes;
    }
    
    public double getPrixTotal(){
        double prix = 0;
        prix = produitsAchetes.stream().map(produit -> produit.getPrixTotal()).reduce(prix, (accumulator, _item) -> accumulator + _item);
        return prix;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public ArrayList<ProduitAchete> getProduitsAchetes() {
        return produitsAchetes;
    }

    public void setProduitsAchetes(ArrayList<ProduitAchete> produitsAchetes) {
        this.produitsAchetes = produitsAchetes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Achat)) return false;
        Achat achat = (Achat) o;
        return getId() == achat.getId() && Double.compare(achat.getRemise(), getRemise()) == 0 && Objects.equals(getDateAchat(), achat.getDateAchat()) && Objects.equals(getProduitsAchetes(), achat.getProduitsAchetes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRemise(), getDateAchat(), getProduitsAchetes());
    }

    @Override
    public String toString() {
        return "Achat{" +
                "id=" + id +
                ", remise=" + remise +
                ", dateAchat=" + dateAchat +
                ", produitsAchetes=" + produitsAchetes +
                '}';
    }
}
