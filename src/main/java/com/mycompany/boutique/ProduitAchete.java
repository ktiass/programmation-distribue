/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.util.Objects;

/**
 *
 * @author TIASS
 */
public class ProduitAchete {
    private int quantite = 1;
    private double remise = 0;
    private Produit produit;

    public ProduitAchete(Produit produit) {
        this.produit = produit;
    }
    
    public ProduitAchete(Produit produit, int quantite, double remise) {
        this.produit = produit;
        this.quantite = quantite;
        this.remise = remise;
    }
    
    public double getPrixTotal(){
        double prix;
        prix = this.produit.getPrixUnitaire() * quantite;
        prix -= prix * remise;
        return prix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProduitAchete)) return false;
        ProduitAchete that = (ProduitAchete) o;
        return quantite == that.quantite && Double.compare(that.remise, remise) == 0 && Objects.equals(produit, that.produit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantite, remise, produit);
    }

    @Override
    public String toString() {
        return "ProduitAchete{" +
                "quantite=" + quantite +
                ", remise=" + remise +
                ", produit=" + produit +
                '}';
    }
}
