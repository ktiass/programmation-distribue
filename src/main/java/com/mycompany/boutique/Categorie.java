/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;
import java.util.*;

/**
 *
 * @author TIASS
 */
public class Categorie {
    private long id;
    private String libelle;
    private String description;
    private ArrayList<Produit> produits;

    public Categorie(long id, String libelle, String description, ArrayList<Produit> produits) {
        this.id = id;
        this.libelle = libelle;
        this.description = description;
        this.produits = produits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Categorie)) return false;
        Categorie categorie = (Categorie) o;
        return id == categorie.id && Objects.equals(libelle, categorie.libelle) && Objects.equals(description, categorie.description) && Objects.equals(produits, categorie.produits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, description, produits);
    }

    @Override
    public String
    toString() {
        return "Categorie{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", description='" + description + '\'' +
                ", produits=" + produits +
                '}';
    }
}
